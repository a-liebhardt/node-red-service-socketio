class SocketLocals {
  constructor() {
    this.socketIOLocals = new Map();

    process.log.debug({
      breadcrumbs: ['socket.io', 'lib', 'SocketLocals'],
      message: 'Init',
    });
  }

  /**
   * Set socket locals
   *
   * @param {*} socketId
   * @param {*} locals
   * @returns boolean
   */
  set(socketId, locals) {
    const defaultLocals = {
      iamUserId: null,
      token: null,
      tokenExpiresIn: 0,
      session: null,
      rooms: new Map()
    }
    locals = { ...defaultLocals, ...locals };

    this.socketIOLocals.set(socketId, locals);

    return true;
  }

  /**
   * Has socket locals
   *
   * @param {*} socketId
   * @returns boolean
   */
  has(socketId) {
    const locals = this.socketIOLocals.get(socketId);
    return locals && locals.iamUserId;
  }

  /**
   * Get socket locals
   *
   * @param {*} socketId
   * @returns locals|null
   */
  get(socketId) {
    const locals = this.socketIOLocals.get(socketId);
    if (locals && locals.iamUserId) {
      return locals;
    }

    return null;
  }

  /**
   * Delete socket locals
   *
   * @param {*} socketId
   * @returns boolean
   */
  delete(socketId) {
    const locals = this.socketIOLocals.get(socketId);

    if (locals && locals.iamUserId) {
      process.iamSocketMap.delete(locals.iamUserId);
    }

    process.log.debug({
      breadcrumbs: ['socket.io', socketId, 'locals', 'unset'],
    });

    return this.socketIOLocals.delete(socketId);
  }
};

module.exports = new SocketLocals();