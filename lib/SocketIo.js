class SocketIo {
  constructor(RED) {
    const { Server } = require('socket.io');
    const corsOrigin = process.env.SOCKETIO_CORS_ORIGINS ? process.env.SOCKETIO_CORS_ORIGINS.split(',') : [];
    const socketEndpoint = process.env.SOCKETIO_ENDPOINT ? process.env.SOCKETIO_ENDPOINT : '/socket.io';

    this.prod = process.env.PRODUCTION ? process.env.PRODUCTION : false;

    let ioOptions = {};
    if (corsOrigin.length) {
      ioOptions['cors']= {
        origin: corsOrigin,
        methods: ['GET', 'POST']
      }
    }

    this.io = new Server(RED.server, ioOptions);
    this.io.serveClient(true);
    this.io.path(socketEndpoint);
    this.io.on('connection', this.handleConnect.bind(this));

    process.log.debug({
      breadcrumbs: ['socket.io', 'lib', 'SocketIo'],
      message: 'Init',
    });
  }

  /**
   *
   * @param {*} socket
   */
  handleConnect(socket) {
    process.log.debug({
      breadcrumbs: ['socket.io', socket.id, 'connection'],
      message: 'Socket connected',
    });

    // Set rules
    for (const ioEvent of this.getEvents()) {
      this.addEventListener(socket, ioEvent);
    }

    socket.on('disconnect', this.handleDisconnect.bind(this, socket));

    socket.connectionTimeout = setTimeout(this.handleConnectTimeout.bind(this, socket), 1000);

    // Request login code by client
    this.io.sockets.sockets.get(socket.id).emit('authenticate', 'a login code is required');

  }

  /**
   *
   * @param {*} socket
   */
   handleConnectTimeout(socket) {
    const self = this;
    const locals = process.socketLocals.get(socket.id);

    // process.log.debug({
    //   breadcrumbs: ['socket.io', socket.id, 'user timeout check'],
    //   message: locals ? 'locals are set' : 'locals are not set',
    //   properties: {
    //     socketId: socket.id
    //   }
    // });

    // If the socket isn't authenticate by now, disconnect
    if (!locals) {
      process.log.debug({
        breadcrumbs: ['socket.io', socket.id, 'authenticate'],
        message: 'Timeout: Not logged, disconnecting socket',
      });
      self.handleDisconnect(socket);
    }

  }

  /**
   *
   * @param {*} socket
   */
  handleDisconnect(socket) {
    process.log.debug({
      breadcrumbs: ['socket.io', socket.id, 'disconnect'],
      message: 'disconnecting',
    });
    this.deleteSocketById(socket.id);
  }

  /**
   * Check if string is json
   *
   * @param {*} str
   * @returns
   */
   isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
  }

  /**
   *
   * @param {*} socket
   * @param {*} error
   */
  couldNotFetchLocals(socket, error) {
    process.log.error({
      breadcrumbs: ['socket.io', socket.id, 'authenticate'],
      message: 'Could not fetch locals',
      properties: error
    });
  }

  /**
   *
   * @param {*} data
   */
  handleListenerEvent(socket, eventId, data) {
    if ('disconnect' === eventId) return;

    process.log.debug({
      breadcrumbs: ['socket.io', socket.id, eventId],
      message: data,
    });

    const self = this;

    if ('authenticate' === eventId) {
      process.emit(eventId, {
        payload: self.isJson(data) ? JSON.parse(data) : data,
        socket: {
          event: eventId,
          id: socket.id,
          iam: null
        }
      });
      return;
    }

    const locals = process.socketLocals.get(socket.id);

    // If the socket isn't authenticate by now, disconnect
    if ('authenticate' !== eventId && !locals) {
      // Request client to submit login code

      process.log.warning({
        breadcrumbs: ['socket.io', socket.id, eventId],
        message: 'Login required',
      });
      return;
    }

    process.iam.getIamConfiguration(socket.id)
      .then(function(iamConfig) {
        if (null === iamConfig) {
          process.log.warning({
            breadcrumbs: ['socket.io', socket.id, eventId],
            message: 'Login expired. New login required',
          });
          self.handleDisconnect(socket);
          return;
        }

        process.emit(eventId, {
          payload: self.isJson(data) ? JSON.parse(data) : data,
          socket: {
            event: eventId,
            id: socket.id
          },
          iam: iamConfig
        });
      });
  };

  /**
   * Automate event listener creation
   *
   * @param {*} socket
   * @param {*} eventId
   */
  addEventListener(socket, eventId) {
    if (!eventId.length) return;

    process.log.debug({
      breadcrumbs: ['socket.io', socket.id, eventId],
      message: 'Bind event listener',
    });

    socket.off(eventId, this.handleListenerEvent.bind(this, socket, eventId));
    socket.on(eventId, this.handleListenerEvent.bind(this, socket, eventId));
  }

  /**
   *
   * @returns array
   */
  getEvents() {
    return process.env.SOCKETIO_EVENTS ? process.env.SOCKETIO_EVENTS.split(',') : [];
  }

  /**
   *
   * @param {*} socketId
   * @returns object
   */
  getSocketById(socketId) {
    return this.io.sockets.sockets.get(socketId);
  }

  /**
   *
   * @param {*} socketId
   */
  deleteSocketById(socketId) {
    process.socketLocals.delete(socketId);

    const socket = this.getSocketById(socketId);
    if (!socket) return;

    clearTimeout(socket.connectionTimeout);
    clearTimeout(socket.iamTokenKeepAliveTimeout);
    socket.disconnect();

    process.log.debug({
      breadcrumbs: ['socket.io', socketId, 'disconnect'],
    });
  }

  /**
   *
   * @param {*} event
   * @param {*} payload
   */
  emit(event, payload) {
    this.io.emit(event, payload);
  }

  /**
   *
   * @param {*} room
   * @param {*} event
   * @param {*} payload
   */
  emitToRoom(room, event, payload) {
    this.io.to(room).emit(event, payload);
  }
};

module.exports = SocketIo;