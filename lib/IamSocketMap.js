class IamSocketMap {
  constructor() {
    this.iamSocketMap = new Map();

    process.log.debug({
      breadcrumbs: ['socket.io', 'lib', 'IamSocketMap'],
      message: 'Init',
    });
  }

  /**
   * Set IAM Socket Link
   *
   * @param {*} iamId
   * @param {*} socketId
   */
  set(iamId, socketId) {
    this.iamSocketMap.set(iamId, socketId);
    return true;
  }

  /**
   * Get IAM Socket Link
   *
   * @param {*} iamId
   * @param {*} socketId
   */
  get(iamId) {
    return this.iamSocketMap.get(iamId);
  }

  /**
   * Check for IAM Socket Link
   *
   * @param {*} iamId
   * @param {*} socketId
   */
  has(iamId) {
    return this.iamSocketMap.has(iamId);
  }

  /**
   * Delete IAM Socket Link
   *
   * @param {*} iamId
   * @param {*} socketId
   */
  delete(iamId) {
    process.log.debug({
      breadcrumbs: ['socket.io', iamId, 'iam socket map', 'unset'],
    });

    return this.iamSocketMap.delete(iamId);
  }
};

module.exports = new IamSocketMap();