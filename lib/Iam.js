class Iam {
  constructor() {
    this.http = require('http');
    process.log.debug({
      breadcrumbs: ['socket.io', 'lib', 'Iam'],
      message: 'Init',
    });
  }

  /**
   * Send IAM request to given endpoint
   *
   * @param {*} endpoint
   * @param {*} payload
   */
   send(endpoint, payload) {
    const self = this;
    return new Promise(async (resolve, reject) => {
      const [ HOST, PORT ] = process.env.IAM_SERVICE_HOST.split(':');
      payload = new URLSearchParams(payload ? payload : {});

      if (!HOST) {
        reject({
          data: 'Invalid http configuration HOST',
          statusCode: -1,
          properties: {
            HOST: HOST,
            PORT: PORT ? PORT : 80
          }
        });
        return;
      }

      const options = {
        hostname: HOST,
        port: PORT ? PORT : 80,
        path: endpoint,
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': payload.toString().length
        }
      }

      const req = self.http.request(options, function(res) {
        let responseData = '';

        res.on('data', function(chunk) {
          responseData += chunk;
        });

        res.on('end', function() {
          if (res.statusCode > 299) {
            resolve({
              data: null,
              statusCode: res.statusCode
            });
            return;
          }

          responseData = JSON.parse(responseData ? responseData.toString('utf-8') : '{}');

          resolve({
            data: responseData,
            statusCode: res.statusCode
          });
        });

        res.on('error', function (error) {
          reject({
            data: 'HTTP error on response',
            statusCode: res.statusCode,
            properties: {
              error: error
            }
          });
        })
      });

      req.on('error', function(error) {
        reject({
          data: 'HTTP error on request',
          statusCode: -1,
          properties: {
            error: error
          }
        });
      });

      req.write(payload.toString());
      req.end();
    });
  }

  /**
   * Logout user by token
   *
   * @param {*} socketId
   */
  logout(socketId) {
    const locals = process.socketLocals.get(socketId);

    const payload = {
      'grant_type': 'refresh_token',
      'client_id': 'account',
      'client_secret': '',
      'refresh_token': `${locals.token}`,
      'scope': 'openid'
    };

    this.send('/auth/realms/demo/protocol/openid-connect/logout', payload)
      .then(function(res) {
        process.log.debug({
          breadcrumbs: ['socket.io', socketId, 'iam', 'logout'],
          message: 'Socket logged out',
          properties: {
            httpStatusCode: res.statusCode,
            data: res.data
          }
        });
      })
      .catch(function(error) {
        if (!msg)
        process.log.error({
          breadcrumbs: ['socket.io', socketId, 'iam', 'logout'],
          message: 'Request error happen',
          properties: error
        });
      });
  }

  async getIamConfiguration(socketId) {
    const locals = process.socketLocals.get(socketId);

    if (!locals) {
      return;
    }

    const payload = {
      'grant_type': 'refresh_token',
      'client_id': 'account',
      'client_secret': '',
      'refresh_token': `${locals.token}`,
      'scope': 'openid'
    };

    const rsp = await this.send('/auth/realms/demo/protocol/openid-connect/token', payload)
      .catch(function(error) {
        process.log.error({
          breadcrumbs: ['socket.io', socketId, 'iam', 'getUserAccessGroups'],
          message: 'Request error happen',
          properties: error
        });
      });

    if (rsp.data) {
      const base64String = rsp.data.access_token.split('.')[1];
      const decodedValue = JSON.parse(Buffer.from(base64String, 'base64').toString('ascii'));
      return {
        username: decodedValue.preferred_username,
        resource_access: decodedValue.resource_access,
        groups: decodedValue.groups
      };
    }

    return null;
  }

  /**
   * Keep alive user token
   *
   * @param {*} socketId
   */
   keepAlive(socketId) {
    const locals = process.socketLocals.get(socketId);
    const socket = process.io.getSocketById(socketId);
    const self = this;

    // process.log.debug({
    //   breadcrumbs: ['socket.io', socketId, 'iam', 'keepAlive'],
    //   message: 'locals',
    //   properties: {
    //     locals: locals
    //   }
    // });

    if (!locals) {
      return;
    }

    const payload = {
      'grant_type': 'refresh_token',
      'client_id': 'account',
      'client_secret': '',
      'refresh_token': `${locals.token}`,
      'scope': 'openid'
    };

    this.send('/auth/realms/demo/protocol/openid-connect/token', payload)
      .then(function(res) {
        if (!res.data) return;

        // Update socket locals
        locals.token = res.data.refresh_token;
        locals.tokenExpiresIn = res.data.refresh_expires_in;
        process.socketLocals.set(socketId, locals);

        // Set token timeout to 95% of recieved expire date and make it milliseconds
        const timeoutInMs = locals.tokenExpiresIn * 0.95 * 1000;
        if (!timeoutInMs || timeoutInMs < 1) {
          process.log.error({
            breadcrumbs: ['socket.io', socketId, 'iam', 'keepAlive'],
            message: 'Invalid timeout value',
            properties: {
              httpStatusCode: res.statusCode,
              locals: locals,
              timeout: timeoutInMs
            }
          });
          return;
        }
        socket.iamTokenKeepAliveTimeout = setTimeout(self.keepAlive.bind(self, socketId), timeoutInMs);
      })
      .catch(function(error) {
        process.log.error({
          breadcrumbs: ['socket.io', socketId, 'iam', 'keepAlive'],
          message: 'Request error happen',
          properties: error
        });
      });
  }

};

module.exports = new Iam();