# node-red-service-socketio
Implementation for [Node-RED](https://nodered.org/) of the popular [Socket.IO](http://socket.io/).

## Installation
To install node-red-contrib-socketio use this command

`npm i https://gitlab.com/a-liebhardt/node-red-service-socketio.git`

## Composition
The Socket.IO implementation is made with
* 1 receive node to listen on a single `topic`
* 1 reply node to send msg.body by send type `emit`, `broadcast.emit`, `room` or `all`. Can also be defined by `msg.payload.body`.
* 1 node to join a Socket IO room
* 1 node to leave a Socket IO room

## Usage
To see an example just run node-red-service

## License
MIT

## Thanks
* [@node-red-contrib-socketio](https://github.com/wperw/node-red-contrib-socketio) for inspiration