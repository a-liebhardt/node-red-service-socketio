module.exports = function (RED) {
  const SocketIo = require('./lib/SocketIo');
  const PROD = process.env.PRODUCTION ? process.env.PRODUCTION : false;
  process.log = require('logger');
  process.io = new SocketIo(RED);
  process.iam = require('./lib/Iam');
  process.iamSocketMap = require('./lib/IamSocketMap');
  process.socketLocals = require('./lib/SocketLocals');

  /**
   * Validate login attempt. If there is already a active socket (in this case iamSocketMap is set) abort new connection and request logout
   *
   * @param {*} msg
   * @param {*} userId
   * @param {*} token
   * @param {*} tokenExpiresIn
   * @param {*} session
   * @returns
   */
  const isValidLoginAttempt = function (msg, userId, token, tokenExpiresIn, session) {
    if ('login' === msg.socket.event && userId && process.iamSocketMap.has(userId)) {
      const mapedSocketId = process.iamSocketMap.get(userId);
      if (mapedSocketId !== msg.socket.id) {
        process.log.warning({
          breadcrumbs: ['socket.io', msg.socket.id, 'double login event'],
          message: 'Prevent double login. Disconnect socket.',
          properties: {
            mapedSocketId: mapedSocketId,
            msg: msg
          }
        });

        const mapedLocals = process.socketLocals.get(mapedSocketId);
        // if (mapedLocals.iamUserId === userId) { }

        let payload = {
          msg: 'Invalid login. Logout requested'
        };

        if (token) {
          process.iam.logout(msg.socket.id);
        }

        // Emit to client only, request logout
        const socket = process.io.getSocketById(msg.socket.id);
        if (socket) {
          socket.emit('login_error', payload);
        }

        return false;
      }
    }

    return true;
  };

  /**
   * Get socket id by parameter or user id
   *
   * @param {*} msg
   * @param {*} userId
   * @returns string|null
   */
  const getSocketId = function (msg, userId) {
    if (msg.socket.id) {
      return msg.socket.id;

    } else if (userId && process.iamSocketMap.has(userId)) {
      return process.iamSocketMap.get(userId);

    }
    return null;
  };

  /**
   * Emit message
   *
   * @param {*} socketId
   * @param {*} msg
   * @returns void
   */
  const emitMsg = function (socketId, msg) {
    if (msg.socket.emit) {
      const socket = process.io.getSocketById(socketId);
      const locals = process.socketLocals.get(socketId);

      switch (msg.socket.emit) {
        case 'broadcast.emit':
          // Emit to all except client
          if (!socket) return;

          process.log.debug({
            breadcrumbs: ['socket.io', socketId, msg.socket.event, msg.socket.emit],
            message: 'broadcast.emit',
            properties: {
              msg: msg
            }
          });
          socket.broadcast.emit(msg.socket.event, msg.payload);
          break;

        case 'emit':
          // Emit to client only
          if (!socket) return;

          process.log.debug({
            breadcrumbs: ['socket.io', socketId, msg.socket.event, msg.socket.emit],
            message: 'emit',
            properties: {
              msg: msg
            }
          });
          socket.emit(msg.socket.event, msg.payload);
          break;

        case 'room':
          // Emit to room members
          if (!msg.payload.room) return;
          if (!locals || !locals.rooms.has(msg.payload.room)) return;

          process.log.debug({
            breadcrumbs: ['socket.io', socketId, msg.socket.event, msg.socket.emit],
            message: 'room',
            properties: {
              msg: msg
            }
          });
          process.io.emitToRoom(msg.payload.room, msg.socket.event, msg.payload);

          break;

        default:
          // Emit to all
          process.log.debug({
            breadcrumbs: ['socket.io', socketId, msg.socket.event, msg.socket.emit],
            message: 'all',
            properties: {
              msg: msg
            }
          });
          process.io.emit(msg.socket.event, msg.payload);
      }
    }
  }

  /**
   * Disconnect socket by socket id
   *
   * @param {*} socketId
   */
  const disconnectSocket = function(socketId) {
    process.io.deleteSocketById(socketId);
  }

  /**
   * Handle emit event
   *
   * @param {*} msg
   * @returns
   */
  const handleEmit = function(msg) {
    const userId = msg.socket?.userId ? msg.socket.userId : null;
    const token = msg.socket?.token ? msg.socket.token : null;
    const tokenExpiresIn = msg.socket?.tokenExpiresIn ? msg.socket.tokenExpiresIn : null;
    const session = msg.socket?.session ? msg.socket.session : null;

    if (!isValidLoginAttempt(msg, userId, token, tokenExpiresIn, session)) return;

    const socketId = getSocketId(msg, userId);

    if (!socketId) {
      process.log.warning({
        breadcrumbs: ['socket.io', socketId, msg.socket.event],
        message: 'socketId is not set',
        properties: {
          msg: msg
        }
      });
      return;
    }

    // Set / Update locals storage
    if (!process.socketLocals.has(socketId) || userId) {
      process.socketLocals.set(socketId, {
        iamUserId: userId,
        token: token,
        tokenExpiresIn: tokenExpiresIn,
        session: session
      });

      if (userId) {
        process.iamSocketMap.set(userId, socketId);
      }
    }

    emitMsg(socketId, msg);

    // Finalize login event
    if (['login'].indexOf(msg.socket.event) > -1) {
      process.iam.keepAlive(socketId);
    }

    // Handle disconnect event
    if (['logout', 'disconnect'].indexOf(msg.socket.event) > -1) {
      process.log.debug({
        breadcrumbs: ['socket.io', socketId, 'disconnect'],
        message: 'disconnected by event',
        properties: {
          msg: msg
        }
      });
      disconnectSocket(socketId);
    }
  }

  /**
   * Handle incoming socket connections
   *
   * @param {*} config
   */
  function socketIoIn(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    // Listen for io events and execute Node-Red event
    config.rules.forEach(function(val, i) {
      process.on(val.v, (msg) => {
        const output = [];
        output[i] = msg;
        node.send(output);
      });
    });
  }

  /**
   * Handle incoming socket event
   *
   * @param {*} config
   */
  function socketIoReceive(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    process.on(config.socketid, (msg) => {
      node.send(msg);
    });
  }

  /**
   * Handle incoming socket emit
   *
   * @param {*} config
   */
  function socketIoReply(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on('input', function (msg) {
      if ('' !== config.socketid) msg.socket.emit = config.socketid;
      if ('room' === config.socketid && config.overwriteroom) msg.payload.body.room = config.room;
      handleEmit(msg);
    });
  }

  /**
   * Handle outgoing emit statements
   *
   * @param {*} config
   */
  function socketIoOut(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on('input', function (msg) {
      handleEmit(msg);
    });
  }

  /**
   * Handle join a room
   *
   * @param {*} config
   */
  function socketIoJoinRoom(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on('input', function (msg) {
      if (config.overwriteroom && '' !== config.room) msg.payload.room = config.room;
      if (!msg.payload.room) return;

      const socket = process.io.getSocketById(msg.socket.id);
      if (!socket) return;

      const locals = process.socketLocals.get(socket.id);
      if (!locals) return;

      if (locals.rooms.has(msg.payload.room)) return;
      locals.rooms.set(msg.payload.room);
      process.socketLocals.set(msg.socket.id, locals);

      socket.join(msg.payload.room);

      process.log.debug({
        breadcrumbs: ['socket.io', msg.socket.id, msg.socket.event, 'room'],
        message: 'join room',
        properties: {
          msg: msg
        }
      });

      node.send(msg);

    });
  }

  /**
   * Handle leave a room
   *
   * @param {*} config
   */
  function socketIoLeaveRoom(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on('input', function (msg) {
      if (config.overwriteroom && '' !== config.room) msg.payload.room = config.room;
      if (!msg.payload.room) return;

      const socket = process.io.getSocketById(msg.socket.id);
      if (!socket) return;

      const locals = process.socketLocals.get(socket.id);
      if (!locals) return;

      if (!locals.rooms.has(msg.payload.room)) return;
      locals.rooms.delete(msg.payload.room);
      process.socketLocals.set(msg.socket.id, locals);

      socket.leave(msg.payload.room);

      process.log.debug({
        breadcrumbs: ['socket.io', msg.socket.id, msg.socket.event, 'room'],
        message: 'leave room',
        properties: {
          msg: msg
        }
      });

      node.send(msg);

    });
  }

  RED.nodes.registerType('socketio-in', socketIoIn);
  RED.nodes.registerType('socketio-out', socketIoOut);
  RED.nodes.registerType('socketio-join', socketIoJoinRoom);
  RED.nodes.registerType('socketio-leave', socketIoLeaveRoom);
  RED.nodes.registerType('socketio-receive', socketIoReceive);
  RED.nodes.registerType('socketio-reply', socketIoReply);
};